package com.darryring.libcore.base;

import android.app.Activity;
import android.os.Bundle;

import com.darryring.libcore.core.AndroidFactory;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by hljdrl on 16/8/3.
 */
public class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidFactory.newFastFactory(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
