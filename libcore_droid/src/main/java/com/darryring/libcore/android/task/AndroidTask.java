package com.darryring.libcore.android.task;

import com.darryring.libcore.task.Task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hljdrl on 15/12/11.
 */
public class AndroidTask implements Task{


    ExecutorService fixedThreadPool;

    public AndroidTask(){
        fixedThreadPool = Executors.newFixedThreadPool(5);
    }
    /**
     * @param _run
     */
    @Override
    public void postTask(Runnable _run) {
        fixedThreadPool.execute(_run);
    }
    /**
     *
     */
    @Override
    public void shutdownTask() {
        fixedThreadPool.shutdown();
        fixedThreadPool = null;
    }

}
