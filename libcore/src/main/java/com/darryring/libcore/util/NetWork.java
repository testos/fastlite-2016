package com.darryring.libcore.util;

/**
 * Created by hljdrl on 16/3/31.
 */
public interface NetWork {
    /**
     * @return
     */
    boolean isNetworkAvailable();

    /**
     * @return
     */
    boolean isWifi();

    /**
     * @return
     */
    boolean is2G();

    /**
     * @return
     */
    boolean is3G();

    /**
     * @return
     */
    boolean is4G();
}
