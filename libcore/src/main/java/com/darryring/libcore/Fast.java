package com.darryring.libcore;

import com.darryring.libcore.cache.DiskCache;
import com.darryring.libcore.cache.KVCache;
import com.darryring.libcore.file.Files;
import com.darryring.libcore.http.Http;
import com.darryring.libcore.logger.Logger;
import com.darryring.libcore.media.Media;
import com.darryring.libcore.task.Task;
import com.darryring.libcore.util.NetWork;

/**
 * Created by hljdrl on 15/12/11.
 */
public class Fast {


    //==========================================================
    public static final String VERSION  =    "1.0.0"         ;
    public static final String NAME     =    "FAST-LITE"     ;
    public static final String TIME     =    "2016-06"          ;
    //==========================================================


    public static   Logger          logger                ;

    public static  KVCache         kvCache               ;

    public static  DiskCache       diskCache             ;
    /**
     * 文件系统
     */
    public static  Files           files                 ;

    public static  Media           media                 ;

    public static  Http            http                  ;
    /**
     * 多线程
     */
    public static  Task            task                  ;

    /**
     * 本地网络工具
     */
    public static  NetWork         network               ;

}
