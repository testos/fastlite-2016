package com.darryring.fast.compat;

import com.darryring.libcore.core.MessageEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by hljdrl on 16/11/4.
 */

public class EventCompat {

    /**
     * @param message
     */
    public static void postEvent(String message){
        EventBus.getDefault().post(new MessageEvent(message));
    }

    /**
     * @param message
     * @param what
     */
    public static void postEvent(String message,int what){
        EventBus.getDefault().post(new MessageEvent(message,what));
    }
}
