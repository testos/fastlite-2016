package com.darryring.fast.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.fast.util.UriForamt;
import com.darryring.libcore.util.StringUtil;
import com.darryring.libmodel.entity.FriendsEntity;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/5/27.
 */
public class FriendsAdapter extends ArrayAdapter<FriendsEntity> {


    LayoutInflater inflater;
    Resources mRes;
    String pkg;
    private String TAG="TAG";
    public FriendsAdapter(Context context, List<FriendsEntity> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
        mRes = context.getResources();
        pkg = context.getPackageName();
    }

    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        FriendsEntity _item = getItem(position);
        ViewTag _mTag = null;
        if (_view == null) {
            _view = inflater.inflate(R.layout.item_listview_friends, null);
            _mTag = new ViewTag();
            _mTag.avator = (SimpleDraweeView) _view.findViewById(R.id.imageview_avator);
            _mTag.name = (TextView) _view.findViewById(R.id.textview_name);
            _mTag.time = (TextView) _view.findViewById(R.id.textview_time);
            _mTag.body = (TextView) _view.findViewById(R.id.textview_body);
            _mTag.grid_images = new ArrayList<ImageView>();
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_1));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_2));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_3));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_4));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_5));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_6));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_7));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_8));
            _mTag.grid_images.add((SimpleDraweeView) _view.findViewById(R.id.grid_image_9));
            _view.setTag(_mTag);
        } else {
            _mTag = (ViewTag) _view.getTag();
        }
        //============================================================================
        _mTag.name.setText(_item.getName());
        _mTag.time.setText(_item.getTime());
        _mTag.body.setText(_item.getBody());
        //----------
        if(StringUtil.isNotNull(_item.getIcon())){
            _mTag.avator.setImageURI(Uri.parse(_item.getIcon()));

        }else{
            Uri _uri =   Uri.parse("res://"+pkg+"/"+R.drawable.ic_user_avator);
            _mTag.avator.setImageURI(_uri);
        }
        //----------
        List<String> imgs = _item.getImages();
        for (int i = 0; i < 9; i++) {

            if (imgs!=null && i < imgs.size()) {
                String _uri = imgs.get(i);
                final ImageView _imageView = _mTag.grid_images.get(i);
                _imageView.setVisibility(View.VISIBLE);
                _imageView.setImageURI(Uri.parse(_uri));
            }else{
                _mTag.grid_images.get(i).setVisibility(View.GONE);
            }
        }
        //============================================================================
        return _view;
    }



    static class ViewTag {
        /**
         * 头像
         */
        public SimpleDraweeView avator;
        /**
         * 名字
         */
        public TextView name;
        /**
         * 时间
         */
        public TextView time;
        /**
         * 正文
         */
        public TextView body;
        /**
         * 图片List
         */
        public List<ImageView> grid_images;

    }
}
