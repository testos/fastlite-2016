package com.darryring.fast.theme;

import android.app.Activity;

import com.darryring.libmodel.entity.theme.ThemeEntity;

/**
 * Created by hljdrl on 16/6/4.
 */
public abstract class FastTheme {
    public static FastTheme fastTheme = null;
    public static void getInstance(Class<? extends FastTheme> _class){
        if(fastTheme==null){
            try {
                fastTheme = _class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
    public abstract void init(Activity t);

    public abstract void setApplyTheme(ThemeEntity e);

    public abstract void setupTheme(Activity t, int themeType);

    public abstract ThemeEntity theme();

}
