package com.darryring.fast.activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.darryring.fast.R;
import com.darryring.fast.theme.FastTheme;
import com.darryring.fast.util.MoviceSize;
import com.darryring.fast.util.QRCodeUtil;
import com.darryring.libcore.Fast;
import com.darryring.libcore.base.BaseAppCompatActivity;
import com.darryring.libmodel.entity.theme.ThemeEntity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 二维码生成页面
 */
public class QRCodeActivity extends BaseAppCompatActivity {
    /**
     * @param _aty
     * @param _qrcode_text
     * @return
     */
    public static Intent buildIntent(Activity _aty,String _qrcode_text){
        Intent _mIm = new Intent(_aty,QRCodeActivity.class);
        _mIm.putExtra(EXTRA_QRCODE_TEXT,_qrcode_text);
        return _mIm;
    }
    public static final String EXTRA_QRCODE_TEXT="extra_qrcode_text";

    private String TAG="QRCodeActivity";
    @BindView(R.id.imageview_qrcode)
    ImageView mQRCodeImageView;
    Bitmap bitmap;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        Fast.logger.i(TAG,"onCreate....");
        setContentView(R.layout.activity_qrcode);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent mIm = getIntent();
        final String _text;
        if(mIm.hasExtra(EXTRA_QRCODE_TEXT)) {
            _text= getIntent().getStringExtra(EXTRA_QRCODE_TEXT);
        }else{
            _text="FastLite";
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MoviceSize _size = MoviceSize.makeHeight(600,600);
                bitmap = QRCodeUtil.createImage(_text,_size.height,_size.height);
                if(bitmap!=null&&mQRCodeImageView!=null){
                    mQRCodeImageView.setImageBitmap(bitmap);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Fast.logger.i(TAG,"onPause....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Fast.logger.i(TAG,"onDestroy....");
        if(mQRCodeImageView!=null){
            mQRCodeImageView.setImageDrawable(new ColorDrawable());
        }
        if(bitmap!=null){
            if(!bitmap.isRecycled()){
                bitmap.recycle();
                bitmap = null;
            }
        }
    }
}
