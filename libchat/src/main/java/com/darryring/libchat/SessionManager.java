package com.darryring.libchat;

import java.util.List;

/**
 * Created by hljdrl on 16/4/12.
 */
public interface SessionManager<T> {

    void init();

    /**
     *
     */
    void loadAllSession();

    /**
     * 获得会话列表
     * @return
     */
    List<T> getSessionList();

    /**
     * 获得一个会话对象
     * @return
     */
    T getSession(String _chatUserId,boolean group);

    /**
     * Post一个回话消息
     * @param t
     */
    void postSession(T t,int _noreadCount);
    /**
     * 重置回话中未读消息总数
     * @param t
     */
    void resetSessionMessageCount(T t);

    void deleteSession(T t);

}
