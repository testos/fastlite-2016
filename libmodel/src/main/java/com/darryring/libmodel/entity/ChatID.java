package com.darryring.libmodel.entity;

/**
 * Created by hljdrl on 16/5/28.
 */
public class ChatID {
    public static final String CHAT_ID_SYSTEM_100 = "100";
    public static final String CHAT_ID_SYSTEM_200 = "200";
    public static final String CHAT_ID_SYSTEM_300 = "300";
    public static final String CHAT_ID_SYSTEM_400 = "400";
    public static final String CHAT_ID_SYSTEM_500 = "500";
    public static final String CHAT_ID_SYSTEM_600 = "600";
    public static final String CHAT_ID_SYSTEM_700 = "700";
    public static final String CHAT_ID_SYSTEM_800 = "800";
    public static final String CHAT_ID_SYSTEM_900 = "900";
    public static final String CHAT_ID_SYSTEM_1000 = "1000";
}
